﻿using System.Web;
using System.Web.Optimization;

namespace phiwiki
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css1").Include(
                      "~/UserTemp/css/font-face.css",
                      "~/UserTemp/vendor/font-awesome-4.7/css/font-awesome.min.css",
                      "~/UserTemp/vendor/font-awesome-5/css/fontawesome-all.min.css",
                      "~/UserTemp/vendor/mdi-font/css/material-design-iconic-font.min.css",
                      "~/UserTemp/vendor/bootstrap-4.1/bootstrap.min.css",
                      "~/UserTemp/vendor/animsition/animsition.min.css",
                      "~/UserTemp/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css",
                      "~/UserTemp/vendor/wow/animate.css",
                      "~/UserTemp/vendor/css-hamburgers/hamburgers.min.css",
                      "~/UserTemp/vendor/slick/slick.css",
                      "~/UserTemp/vendor/select2/select2.min.css",
                      "~/UserTemp/vendor/perfect-scrollbar/perfect-scrollbar.css",
                      "~/UserTemp/css/theme.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                      "~/UserTemp/vendor/jquery-3.2.1.min.js",
                      "~/UserTemp/vendor/bootstrap-4.1/popper.min.js",
                      "~/UserTemp/vendor/bootstrap-4.1/bootstrap.min.js",
                      "~/UserTemp/vendor/slick/slick.min.js",
                      "~/UserTemp/vendor/wow/wow.min.js",
                      "~/UserTemp/vendor/animsition/animsition.min.js",
                      "~/UserTemp/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js",
                      "~/UserTemp/vendor/counter-up/jquery.waypoints.min.js",
                      "~/UserTemp/vendor/counter-up/jquery.counterup.min.js",
                      "~/UserTemp/vendor/circle-progress/circle-progress.min.js",
                      "~/UserTemp/vendor/perfect-scrollbar/perfect-scrollbar.js",
                      "~/UserTemp/vendor/chartjs/Chart.bundle.min.js",
                      "~/UserTemp/vendor/select2/select2.min.js",
                      "~/UserTemp/js/main.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css2").Include(
                      "~/LandingTemplate/css/linearicons.css",
                      "~/LandingTemplate/css/font-awesome.min.css",
                      "~/LandingTemplate/css/bootstrap.css",
                      "~/LandingTemplate/css/magnific-popup.css",
                      "~/LandingTemplate/css/nice-select.css",
                      "~/LandingTemplate/css/animate.min.css",
                      "~/LandingTemplate/css/owl.carousel.css",
                      "~/LandingTemplate/css/main.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/js2").Include(
                      "~/LandingTemplate/js/vendor/jquery-2.2.4.min.js",
                      "~/LandingTemplate/js/vendor/bootstrap.min.js",
                      "~/LandingTemplate/text/javascript",
                      "~/LandingTemplate/js/easing.min.js",
                      "~/LandingTemplate/js/hoverIntent.js",
                      "~/LandingTemplate/js/superfish.min.js",
                      "~/LandingTemplate/js/jquery.ajaxchimp.min.js",
                      "~/LandingTemplate/js/jquery.magnific-popup.min.js",
                      "~/LandingTemplate/js/owl.carousel.min.js",
                      "~/LandingTemplate/js/jquery.sticky.js",
                      "~/LandingTemplate/js/jquery.nice-select.min.js",
                      "~/LandingTemplate/js/parallax.min.js",
                      "~/LandingTemplate/js/waypoints.min.js",
                      "~/LandingTemplate/js/jquery.counterup.min.js",
                      "~/LandingTemplate/js/mail-script.js",
                      "~/LandingTemplate/js/main.js"
                      ));
        }
    }
}
