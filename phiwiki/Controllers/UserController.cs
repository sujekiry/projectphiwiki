﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using phiwiki.Models;
using phiwiki;

namespace phiwiki.Controllers
{
    public class UserController : Controller
    {

        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Registration()
        {
            tbl_user regis = new tbl_user();
            return View(regis);
        }

        [HttpPost]
        public ActionResult Registration(tbl_user r)
        {
            var b = db.tbl_user.Where(x => x.username.Equals(r.username)).FirstOrDefault();
            if (b != null)
            {
                TempData["error"] = "user name already exists !";
                TempData.Keep();
                return RedirectToAction("Registration");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    r.status = "No";
                    r.fk_id_role = 1;
                    db.tbl_user.Add(r);
                    db.SaveChanges();
                    SendMail(r.email);


                    TempData["success"] = "check your email to confirm your account !";
                    TempData.Keep();
                    return RedirectToAction("Registration");
                }

                return View(r);
            }
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(tbl_user j)
        {
            
            var b = db.tbl_user.AsEnumerable().Where(x => x.username.Equals(j.username) && Encryptor.DecryptString(x.password_user).Equals(j.password_user)).FirstOrDefault();
            if (b != null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                TempData["failed"] = "Incorrect user name or password !";
                TempData.Keep();
                return RedirectToAction("Login");
            }
        }

        public ActionResult Forgetpass()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Forgetpass(string email)
        {
            var dataadmin = db.tbl_user.Where(x => x.email == email).FirstOrDefault();
            if (dataadmin != null)
            {
                try
                {
                    dataadmin.status = "Yes";
                    db.SaveChanges();
                    SendMailForgot(dataadmin.email, dataadmin.id_user);
                    TempData["cek"] = "Cek Your E-mail";
                    TempData.Keep();
                    return RedirectToAction("Forgetpass");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return RedirectToAction("Forgetpass");
                }
            }
            else
            {
                TempData["cek"] = "Email Wrong";
                TempData.Keep();
                return RedirectToAction("Forgetpass");
            }
            //return View();

        }

        public ActionResult Resetpass(string id)
        {
            int decrypt = Convert.ToInt32(Encryptor.DecryptString(id));
            tbl_user objUser = db.tbl_user.Find(decrypt);
            if (objUser.status == "Yes")
            {
                TempData["reset"] = decrypt;
                TempData.Keep();
                return View();
            }

            return RedirectToAction("Login", "User");
        }

        [HttpPost]
        public ActionResult Resetpass(tbl_user j, int id_user)
        {
            //if (j.password_user == j.confirmpassword)
            //{
            var data = db.tbl_user.Find(id_user);
            data.password_user = Encryptor.EncrpytString(j.password_user);
            db.SaveChanges();
            data.status = "No";
            db.SaveChanges();
            return RedirectToAction("Login");

            //}
            //else
            //{
            //    TempData["valid"] = "Password Don't Match";
            //}

            //return View();
        }

        

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public void SendMailForgot(string email, int id)
        {
            if (ModelState.IsValid)
            {

                var fromAddress = new MailAddress("mdinarlatiffa32@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "hannya23";
                const string subject = "Phiwiki - Reset Password";
                string Encrypt = Encryptor.EncrpytString(id.ToString());
                //var body = "Click here to change your password : <a href='https://localhost:44349/Auth/NewPassword/" + objForgot.id_forgot_password + "'>Link</a>";

                var body = "Hi, " +
                    "<br/>Thanks for signing up to Phiwiki! " +
                    "<br/>" +
                    "<br/>" +
                    "<br/>To get started, click the link below to confirm your account" +
                    "<br/> <a href=" + "https://localhost:44319/User/Resetpass?id=" + Encrypt + ">Link</a>" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }

        [HttpPost]
        public void SendMail(string email)
        {

            if (ModelState.IsValid)
            {
                var fromAddress = new MailAddress("mdinarlatiffa32@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "hannya23";
                const string subject = "Phiwiki - Regristration";
                //string Encrypt = Encryptor.EncrpytString(objForgot.id_forgot_password.ToString());
                //var body = "Click here to change your password : <a href='https://localhost:44349/Auth/NewPassword/" + objForgot.id_forgot_password + "'>Link</a>";
                var body = "Hi, " +
                    "<br/>Thanks for signing up to Phiwiki! " +
                    "<br/>" +
                    "<br/>" +
                    "<br/>To get started, click the link below to confirm your account" +
                    "<br/> <a href='https://localhost:44319/User/Resetpass'>Link</a>" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }        
    }
}
