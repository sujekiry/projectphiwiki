﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace phiwiki.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult MasterAnggota()
        {
             return View();
        }

        public ActionResult MasterQuiz()
        {
            return View();
        }

        public ActionResult MasterTimeline()
        {
            return View();
        }
        public ActionResult MasterPembelian()
        {
            return View();
        }
        public ActionResult MasterVideo()
        {
            return View();
        }
        public ActionResult MasterPengaduan()
        {
            return View();
        }
        public ActionResult MasterRating()
        {
            return View();
        }
        public ActionResult MasterTestimoni()
        {
            return View();
        }
    }
}