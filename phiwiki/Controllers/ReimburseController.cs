﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using phiwiki.Models;

namespace phiwiki.Controllers
{
    public class ReimburseController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Reimburse()
        {
            tbl_reimburse reimburse = new tbl_reimburse();
            return View(reimburse);
        }

        [HttpPost]
        public ActionResult Reimburse(tbl_reimburse r)
        {
           
                if (ModelState.IsValid)
                {
                    
                    
                    db.tbl_reimburse.Add(r);
                    db.SaveChanges();
                    TempData["successful"] = "Order successful";
                    TempData.Keep();
                return RedirectToAction("Reimburse");
                }            
           
            return View(r);            

        }



        // GET: Reimburse
        public ActionResult Index()
        {
            return View();
        }

        // GET: Reimburse/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Reimburse/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reimburse/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Reimburse/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Reimburse/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Reimburse/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Reimburse/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
